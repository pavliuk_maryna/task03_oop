package com.epam.pavliuk;

import com.epam.pavliuk.controller.FlowerService;
import com.epam.pavliuk.controller.ShopService;
import com.epam.pavliuk.model.Flower;
import com.epam.pavliuk.model.Pot;
import com.epam.pavliuk.model.PotFlower;
import com.epam.pavliuk.model.Rack;
import com.epam.pavliuk.model.Shop;
import com.epam.pavliuk.view.menu.BouquetMenu;
import com.epam.pavliuk.view.menu.ShopMenu;
import java.util.HashMap;

public class FlowerShopApplication {

  public static void main(String[] args) {
    // population of shop with flowers
    Shop shop = new Shop(new Rack(new HashMap<>()));
    shop.getRack().getFlowers().put("1", new Flower("rose", "red", 100, 15, 10.2));
    shop.getRack().getFlowers().put("2", new Flower("chamomile", "yellow", 80, 12, 6.25));
    shop.getRack().getFlowers().put("3", new Flower("lily", "violet", 70, 3, 9.15));
    shop.getRack().getFlowers()
        .put("4", new PotFlower("cactus", "green", 20, 0, 29.15, new Pot("plastic", "blue", 10)));

    ShopService shopService = new ShopService(shop);
    FlowerService flowerService = new FlowerService();
    ShopMenu shopMenu = new ShopMenu(shopService, new BouquetMenu(flowerService, shopService));

    shopMenu.open();
  }
}
