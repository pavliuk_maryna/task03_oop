package com.epam.pavliuk.controller;

import com.epam.pavliuk.model.Bouquet;
import com.epam.pavliuk.model.Flower;
import java.util.ArrayList;
import java.util.Collections;

public class FlowerService {

  private Bouquet bouquet;

  public FlowerService() {
    this.bouquet = new Bouquet(new ArrayList<>());
  }

  public Bouquet getBouquet() {
    return bouquet;
  }

  public void setBouquet(Bouquet bouquet) {
    this.bouquet = bouquet;
  }

  public void addFlower(Flower flower) {
    bouquet.getFlowers().add(flower);
  }

  public void printBouquet() {
    System.out.println(bouquet.toString());
  }

  public void sortBouquet() {
    Collections.sort(bouquet.getFlowers());
  }
}
