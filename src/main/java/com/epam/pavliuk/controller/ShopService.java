package com.epam.pavliuk.controller;

import com.epam.pavliuk.model.Flower;
import com.epam.pavliuk.model.Shop;

public class ShopService {

  private Shop shop;

  public ShopService(Shop shop) {
    this.shop = shop;
  }

  public void printAllFlowers() {
    for (String key : shop.getRack().getFlowers().keySet()) {
      System.out.println("   "+key +" - "+shop.getRack().getFlowers().get(key));
    }
  }

  public Shop getShop() {
    return shop;
  }

  public void setShop(Shop shop) {
    this.shop = shop;
  }
}
