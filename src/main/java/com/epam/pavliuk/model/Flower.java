package com.epam.pavliuk.model;

public class Flower implements Comparable<Flower> {

  private String name;
  private String color;
  private int cmLength;
  private int numberOfLeaves;
  private double price;

  public Flower(String name, String color, int cmLength, int numberOfLeaves, double price) {
    this.name = name;
    this.color = color;
    this.cmLength = cmLength;
    this.numberOfLeaves = numberOfLeaves;
    this.price = price;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getColor() {
    return color;
  }

  public void setColor(String color) {
    this.color = color;
  }

  public int getCmLength() {
    return cmLength;
  }

  public void setCmLength(int cmLength) {
    this.cmLength = cmLength;
  }

  public int getNumberOfLeaves() {
    return numberOfLeaves;
  }

  public void setNumberOfLeaves(int numberOfLeaves) {
    this.numberOfLeaves = numberOfLeaves;
  }

  public double getPrice() {
    return price;
  }

  public void setPrice(double price) {
    this.price = price;
  }

  @Override
  public int compareTo(Flower flower) {
    if (price == flower.price) {
      return 0;
    } else if (price > flower.price) {
      return 1;
    } else {
      return -1;
    }
  }
  
  @Override
  public String toString() {
    return "name: " + name + " - color: "
        + color + " - length: "
        + cmLength + " - numberOfLeaves: "
        + numberOfLeaves + " - price: " + price;
  }

}
