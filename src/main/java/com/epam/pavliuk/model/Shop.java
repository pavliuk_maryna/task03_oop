package com.epam.pavliuk.model;

public class Shop {

 private Rack rack;

  public Shop(Rack rack) {
    this.rack = rack;
  }

  public Rack getRack() {
    return rack;
  }

  public void setRack(Rack rack) {
    this.rack = rack;
  }
}
