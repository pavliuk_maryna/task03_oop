package com.epam.pavliuk.model;

public class PotFlower extends Flower {

  private Pot pot;

  public PotFlower(String name, String color, int length, int numberOfLeaves, double price,
      Pot pot) {
    super(name, color, length, numberOfLeaves, price);
    this.pot = pot;
  }

  public Pot getPot() {
    return pot;
  }

  public void setPot(Pot pot) {
    this.pot = pot;
  }

  @Override
  public String toString() {
    return super.toString()+" - pot: "+ pot;
  }
}
