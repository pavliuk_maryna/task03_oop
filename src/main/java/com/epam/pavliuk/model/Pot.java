package com.epam.pavliuk.model;

public class Pot {

  private String material;
  private String color;
  private int sizeSquareCm;

  public Pot(String material, String color, int sizeSquareCm) {
    this.material = material;
    this.color = color;
    this.sizeSquareCm = sizeSquareCm;
  }

  public String getMaterial() {
    return material;
  }

  public void setMaterial(String material) {
    this.material = material;
  }

  public String getColor() {
    return color;
  }

  public void setColor(String color) {
    this.color = color;
  }

  public int getSizeSquareCm() {
    return sizeSquareCm;
  }

  public void setSizeSquareCm(int sizeSquareCm) {
    this.sizeSquareCm = sizeSquareCm;
  }

  @Override
  public String toString() {
    return "{ " +
        "material: " + material +
        " - color: " + color +
        " - sizeSquareCm: " + sizeSquareCm +
        " }";
  }
}
