package com.epam.pavliuk.model;

import java.util.List;

public class Bouquet {

  private List<Flower> flowers;

  public Bouquet(List<Flower> flowers) {
    this.flowers = flowers;
  }

  public List<Flower> getFlowers() {
    return flowers;
  }

  public void setFlowers(List<Flower> flowers) {
    this.flowers = flowers;
  }

  @Override
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("My bouquette: \n");
    for (Flower flower : flowers) {
      stringBuilder.append(flower.toString()+ "\n");
    }
    return stringBuilder.toString();
  }
}
