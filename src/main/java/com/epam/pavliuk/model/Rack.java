package com.epam.pavliuk.model;

import java.util.List;
import java.util.Map;

public class Rack {

  private Map<String,Flower> flowers;

  public Rack(Map<String, Flower> flowers) {
    this.flowers = flowers;
  }

  public Map<String, Flower> getFlowers() {
    return flowers;
  }

  public void setFlowers(Map<String, Flower> flowers) {
    this.flowers = flowers;
  }
}
