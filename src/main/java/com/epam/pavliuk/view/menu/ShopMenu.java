package com.epam.pavliuk.view.menu;

import com.epam.pavliuk.controller.ShopService;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ShopMenu {

  private ShopService shopService;
  private BouquetMenu bouquetMenu;
  private Scanner scanner;
  private List<String> menuItems;

  public ShopMenu(ShopService shopService, BouquetMenu bouquetMenu) {
    this.shopService = shopService;
    this.bouquetMenu = bouquetMenu;
    scanner = new Scanner(System.in);
    menuItems = new ArrayList<>();
    menuItems.add("1 - display all flowers in shop");
    menuItems.add("2 - open bouquet menu");
    menuItems.add("exit - for exit");
  }

  public void open() {
    while (true) {
      for (String menuItem : menuItems) {
        System.out.println(menuItem);
      }
      String choice = scanner.next();
      if (choice.equals("1")) {
        System.out.println("There are such flowers:");
        shopService.printAllFlowers();
      } else if (choice.equals("2")) {
        bouquetMenu.open();
      } else {
        break;
      }
    }
  }
}
