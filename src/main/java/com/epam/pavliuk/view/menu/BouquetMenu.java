package com.epam.pavliuk.view.menu;

import com.epam.pavliuk.controller.FlowerService;
import com.epam.pavliuk.controller.ShopService;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class BouquetMenu {

  private FlowerService flowerService;
  private ShopService shopService;

  private Scanner scanner;
  private List<String> menuItems;

  public BouquetMenu(FlowerService flowerService, ShopService shopService) {
    this.flowerService = flowerService;
    this.shopService = shopService;
    scanner = new Scanner(System.in);
    menuItems = new ArrayList<>();
    menuItems.add("1 - print current bouquet");
    menuItems.add("2 - add flower");
    menuItems.add("3 - sort bouquet flowers by price");
    menuItems.add("back - to go back");
  }

  public void open() {
    while (true) {
      for (String menuItem : menuItems) {
        System.out.println(menuItem);
      }
      String choice = scanner.next();
      if (choice.equals("1")) {
        flowerService.printBouquet();
      } else if (choice.equals("2")) {
        while (true) {
          shopService.printAllFlowers();
          System.out.println("Choose flower or type back to return");
          String flowerChoice = scanner.next();
          if (flowerChoice.equals("back")) {
            break;
          } else {
            flowerService.addFlower(shopService.getShop().getRack().getFlowers().get(flowerChoice));
          }
        }
      } else if(choice.equals("3")) {
        flowerService.sortBouquet();
      } else {
        break;
      }
    }
  }
}
